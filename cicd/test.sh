#!/bin/bash


test_prime() {
    expected="$2"
    output=$(echo $1 | ./PrimeNumber)
    if [ "$output" == "$expected" ]; then
        echo "Test passed: $1 is a prime number as expected."
    else
        echo "Test failed: Expected '$expected', but got '$output'."
    fi
}

cd usr/bin

test_prime "2" "Enter a number to check if it is prime: 2 is a prime number."
test_prime "3" "Enter a number to check if it is prime: 3 is a prime number."
test_prime "4" "Enter a number to check if it is prime: 4 is not a prime number."
test_prime "5" "Enter a number to check if it is prime: 5 is a prime number."
test_prime "9" "Enter a number to check if it is prime: 9 is not a prime number."
