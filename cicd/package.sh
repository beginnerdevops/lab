#!/bin/bash

# Обновляем пакеты и устанавливаем необходимые инструменты
apt update
apt install -y dpkg-dev devscripts

# Переходим на уровень выше
cd ..

# Устанавливаем права доступа для файлов в директории lab/
chmod 0755 lab/*
chmod 0755 lab/DEBIAN/postinst

# Создаем пакет .deb
dpkg-deb --build ./lab

# Копируем пакет .deb в директорию lab/
cp ./lab.deb ./lab