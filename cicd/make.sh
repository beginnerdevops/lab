#!/bin/bash

# Создаем директорию usr/bin/
mkdir -p usr/bin/

# Переходим в директорию src/
cd src

# Выполняем сборку
make all

# Копируем исполняемый файл в директорию usr/bin/
cp ./PrimeNumber ../usr/bin/